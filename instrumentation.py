#!/usr/bin/python3

import psycopg2 as pg
import os
from itertools import groupby

mbz = pg.connect(dbname='musicbrainz_db', host='mb', user='musicbrainz')

sql = """with recs as (select distinct on (recording.gid) par.gid as par, recording.id as rid, recording.gid as rgid, work.id as wid, work.gid as wgid, work.name as wname, recording.name as rname, work.comment as wcomm
              from recording
              join l_recording_work on l_recording_work.entity0=recording.id
              join work on l_recording_work.entity1=work.id
              join l_artist_work on l_artist_work.entity1=work.id
              join artist on l_artist_work.entity0=artist.id
              left join l_work_work lww on lww.entity1=work.id
              left join work par on lww.entity0=par.id
              where artist.gid='819eaeb2-8dd8-48a5-ad07-0bcd137985ef')
select recs.par, recs.rgid, recs.rname, recs.wgid, recs.wname, array_agg(lt.name || coalesce(': '||lat.name, '') order by lt.name, lat.name) as inst, recs.wcomm
from recs
join l_artist_recording lar on lar.entity1=recs.rid
join link l on lar.link=l.id
join link_type lt on l.link_type=lt.id
left join link_attribute la on la.link=l.id
left join link_attribute_type lat on lat.id=la.attribute_type
where lt.name in ('performing orchestra', 'performer', 'instrument', 'vocal', 'conductor', 'arranger')
and coalesce(lat.name,'') not in ('solo','additional','guest')
group by recs.par, recs.rgid, recs.rname, recs.wgid, recs.wname, recs.wcomm
order by recs.wgid;"""

def numb(n):
    if n>1:
        return "{}x ".format(n)
    else:
        return ""

def pretty_instrument(inst):
    inst=inst.replace("instrument: ", "")
    inst=inst.replace("vocal: choir vocals", "choir")
    if inst[:7]=="vocal: ":
        inst="vocals"
    inst=inst.replace("performing orchestra", "orchestra")
    return inst

def instrumentation(lst):
    r=", ".join(sorted(list({ "{}{}".format(numb(lst.count(i)), i) for i in lst})))
    return(r)

def recording_html(name, gid):
    return("""<a href="https://musicbrainz.org/recording/{gid}">{name}</a>""".format(gid=gid,name=name))

def work_html(gid):
    comment = comments.get(gid,"")
    return("""<a href="https://musicbrainz.org/work/{gid}">{name}</a> <div class="comment">{comment}</div>""".format(gid=gid,name=works[gid],comment=comment))

def inst_html(inst,data):
    n=len(data)
    return ("""<a>{i} ({n})</a>""".format(i=inst,n=n))

with mbz.cursor() as cur:
    cur.execute(sql)
    result=cur.fetchall()
    composer = [ [r[0],r[3],r[4],instrumentation([pretty_instrument(i) for i in r[5]]),recording_html(r[2],r[1]),r[6]] for r in result ]

works = { w[1]:w[2] for w in composer }
comments = { w[1]:w[5] for w in composer if w[5] }

grouped=dict()

for k, g in groupby(sorted(composer, key=lambda i: list([i[1],i[3]])), lambda i: list([i[1],i[3]])):
    grouped.setdefault(k[0],dict())
    for i in g:
        grouped[k[0]].setdefault(k[1],list())
        grouped[k[0]][k[1]].append(i[4])

def rec_output(rec,fil):
    print("<li>{rec}</li>".format(rec=rec), file=fil)

def inst_output(data,fil):
    print("""<ul class="collapsibleList">""",file=fil)
    for k,v in sorted(data.items(),key=lambda i:list([len(i[1]),i[0]]), reverse=True ):
        print("""<li>{inst}""".format(inst=inst_html(k,v)), file=fil)
        print("""<ul>""", file=fil)
        for rec in v:
            rec_output(rec,fil)
        print("</ul></li>", file=fil)
    print("</ul>",file=fil)

def header(fil):
    print("""<html>
<head>
<meta charset="utf-8">
<title>Works by instrument</title>
<script>
//http://code.stephenmorley.org/javascript/collapsible-lists/
var CollapsibleLists=function(){function e(b,c){[].forEach.call(b.getElementsByTagName("li"),function(a){c&&b!==a.parentNode||(a.style.userSelect="none",a.style.MozUserSelect="none",a.style.msUserSelect="none",a.style.WebkitUserSelect="none",a.addEventListener("click",g.bind(null,a)),f(a))})}function g(b,c){for(var a=c.target;"LI"!==a.nodeName;)a=a.parentNode;a===b&&f(b)}function f(b){var c=b.classList.contains("collapsibleListClosed"),a=b.getElementsByTagName("ul");[].forEach.call(a,function(a){for(var d=a;"LI"!==d.nodeName;)d=d.parentNode;d===b&&(a.style.display=c?"block":"none")});b.classList.remove("collapsibleListOpen");b.classList.remove("collapsibleListClosed");0<a.length&&b.classList.add("collapsibleList"+(c?"Open":"Closed"))}return{apply:function(b){[].forEach.call(document.getElementsByTagName("ul"),function(c){c.classList.contains("collapsibleList")&&(e(c,!0),b||[].forEach.call(c.getElementsByTagName("ul"),function(a){a.classList.add("collapsibleList")}))})},applyTo:e}}();
var runOnLoad=function(c,o,d,e){function x(){for(e=1;c.length;)c.shift()()}o[d]?(document[d]('DOMContentLoaded',x,0),o[d]('load',x,0)):o.attachEvent('onload',x);return function(t){e?o.setTimeout(t,0):c.push(t)}}([],window,'addEventListener');
runOnLoad(CollapsibleLists.apply);
</script>
</head>
<body>
<style>
li.collapsibleListOpen{
  cursor           : pointer;
}
li.collapsibleListClosed{
  cursor           : pointer;
}
</style>
""",file=fil)

def work_output(gid,data,fil):
    print("""<li class="workitem">{work}""".format(work=work_html(gid)), file=fil)
    inst_output(data,fil)
    print("</li>",file=fil)


with open("output/dvorak_instruments.html","w") as fil:
    header(fil)
    print("""<ul class="list">""", file=fil)
    for k,v in sorted(grouped.items(),key=lambda i: list([works[i[0]],comments.get(i[0],"")])):
        work_output(k,v,fil)
    print("</ul>", file=fil)
        
    
