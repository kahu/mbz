#!/usr/bin/python3

import psycopg2 as pg

### name, release_groups, releases, tracks, recordings

header = """<html>
<head>
<meta charset="utf-8">
<title>Composers</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.15/js/jquery.tablesorter.js"></script>
<script>
$(function(){
  $("#derp").tablesorter();
});
</script>
</head>
<body>
<table id="derp" class="herp"><thead><tr><th>Artist</th><th>Release Groups</th><th>Releases</th><th>Tracks</th><th>Recordings</th><th>Shittiness</th></tr></thead><tbody>
"""

tables = ["release_group", "release", "track", "recording"]

composers = ['24f1766e-9635-4d58-a4d4-9413f9f98a4c','b972f589-fb0e-474e-b64a-803b0364fa75','1f9df192-a621-4f54-8850-2c5373b7eac9','c70d12a2-24fe-4f83-a6e6-57d84f8efb51','f91e3a88-24ee-4563-8963-fab73d2765ed','ad79836d-9849-44df-8789-180bbc823f3c','9ddd7abc-9e1b-471d-8031-583bc6bc8be9','09ff1fe8-d61c-4b98-bb82-18487c74d7b7','c130b0fb-5dce-449d-9f40-1437f889f7fe','be50643c-0377-4968-b48c-47e06b2e2a3b','27870d47-bb98-42d1-bf2b-c7e972e6befc','819eaeb2-8dd8-48a5-ad07-0bcd137985ef','519dd32e-8f30-4380-8826-7aa99169e1bb','3cd3882c-00f8-4362-a0c2-ad89ed248533','691b0e9d-9e57-41cf-932d-a3d21b068e75','8d610e51-64b4-4654-b8df-064b0fb7a9d9','6fdd3b3e-1ea6-4da9-8d6f-8f8de01c133a','0e85eb79-1c05-44ba-827c-7b259a3d941a','44b16e44-da77-4580-b851-0d765904573e','2cd475bb-1abd-40c4-9904-6d4b691c752c','0e43fe9d-c472-4b62-be9e-55f971a023e1','c278de2c-9696-4fdf-a919-0781cd945e2c','eefd7c1e-abcf-4ccc-ba60-0fd435c9061f','fd14da1b-3c2d-4cc8-9ca6-fc8c62ce6988','4cb43d82-824e-4034-b03d-1a98f36f6e16','9debb4c3-dc10-440f-9c96-b777d51ea998','f1bedf1f-4445-4651-9c35-f4a3f3860a13','49ae5227-605a-47a8-9b8e-cd89bf01a97c','4137c070-15b2-4d00-a9f0-3517d02a9ba8','013c8e5b-d72a-4cd3-8dee-6c64d6125823','4e60a56a-514a-4a19-a3cc-49927c96b3cb','8f831f50-e409-47c3-8598-71a61bc8cfb3','846be3c9-5f94-46ab-97b9-531335dd3658','4f3b96ed-f1f1-4a68-be73-0e0657837096','274774a7-1cde-486a-bc3d-375ec54d552d','fa19a8b6-e7f4-40d4-af15-7a7c41ac7d8f','65744963-191a-44ef-a3c7-b693a808a158','c1380b12-3909-47d8-a5fd-d20e76123310','9a33a7ad-f725-473a-b50a-1d1796183af1','9b490b96-ad82-4d7b-9055-f0a196ad64cc','ddea5540-2c7d-4266-8507-b367c2635d35','a693df4a-8762-4a45-b7fd-6695b558385e','f6486fc7-90ef-48ad-8ca3-905af1354afe','8255db36-4902-4cf6-8612-0f2b4288bc9a','c6e5c5f4-984e-432a-87b8-b6afa288bca1','4cfe7051-f649-4d07-83b3-7a732abe7249','edfced8a-01bc-4c22-96b2-da58edd6b8af','69802473-e361-4e15-bbd8-53fe83cd1587','95c7a1d1-5cfd-41d5-99f9-0611262de8e1','e1d521ea-5b97-4981-987c-ba988b2a87d7','4068eaba-ca03-4c34-9172-2b709b8050e6','a4eba7b5-aceb-4acf-b297-3c0a3cd42757','5e1ef22b-310a-46ad-885b-4897b8c9c85a','9bd20e6a-b41d-4983-a5b6-c0ff849fa235','6eb5695e-cf7b-4b94-b441-a09901127ef9','5c1a3f8f-d5e5-4dcd-9e44-9443f06bb77d','3ba68671-e3b5-4263-81dc-76b16b29bbc6','f106fb52-0b54-4135-bdfd-9ccfd7e320cb','74ed34ce-ee95-44e9-a87d-4d2d5056c24a','aad3af83-5b59-4b86-a569-1a8409149b09','2382cbc9-dd4e-4fc8-a92e-5391f70bd3b2','e3d5cfcd-e42b-411d-add2-716a5029f59b','a8dbf22f-9788-4cf6-a0df-c9244384f295','788c380d-247b-42f0-b7a9-64881e1f0fd9','d581b2f2-f945-4566-a249-2d0171466993','f61c909a-db95-4a61-bc23-65a86e0d2907','b29a3b55-785c-4072-9daf-0153cac864b6','5a0988a4-695c-4bff-bc68-4f312427495e','51f5d4c3-c69a-4203-9281-d585e32aa7ef','a3546087-fe59-47d3-a3a8-42d58e2573c7','a81e92aa-7580-4201-a0c3-d66b420695c4','af7c27c0-d1ef-4113-bbd6-63e97963f434','c2d17829-1424-435b-9386-c77d3a920abe','0b3ee6cd-a2df-4144-adf9-7807ee7ecc4f','1be1367d-119f-4b08-bdfe-50b95043e544','eb87b85e-857d-4c11-8aa9-4c55f3b54880','0a46cf2a-61bd-447d-b8fd-a2b32eb20282','560b5e65-8d53-41b4-9913-83368f4721a0','17268036-f51c-427c-a8a2-241fac69d39f','fa25cd1f-beeb-4718-b4bb-d3da4f53539f','27bc8cf5-72e1-4888-87de-259fbcaac3ff','86523090-6085-42c1-80b7-772f1bc80472','cbdcee87-59a0-4f05-929b-86a3d91b6dfa','83903121-f611-4875-984c-673ae7173e56','bb94bbf5-b2b3-4a5a-bcab-7ba8b60cccea','dbe921e2-a7a7-4d4c-b2f8-109da1cd1e3a','4fddc10d-b218-43b7-8d8d-65500e1402d0','0e738f68-783c-4a6a-80ae-4f7500861060','30f53666-aa3b-44c1-80c1-4d09929e28e8','76325a9d-6c25-4649-96b1-84e9b99d6b4b','96c39679-7de4-48d1-a9ea-d8840296bb73','b8ecf204-6be8-4041-ab11-6d9971ea2711','e378c6b3-7d74-48b5-ab04-9aac0df7562c','e2eb5293-00fb-40bb-92f4-3db07a229118','f7ef501b-2bc0-4083-ac52-3518255883a2','e91fbf2b-178d-4243-972e-73eff6a5676f','4daeb526-6c4c-4f77-bdf5-4a0fcb714bfe','7c6ef060-fe44-4c66-ae6d-a86791abaf09','7be0b588-030c-4904-836f-71ff43a2dfec','66197dbc-c16b-4c95-9e1b-cc617783cb8c']

query = """select a.gid, count(*) from {table} butt
           join artist_credit_name acn on butt.artist_credit=acn.artist_credit
           join artist a on acn.artist=a.id
           where a.gid in %s
           group by a.gid order by count(*) desc;"""

rec_query = """select a.gid, count(*) from recording rec
               join artist_credit_name acn on rec.artist_credit=acn.artist_credit
               join artist a on acn.artist=a.id
               where a.gid in %s
               and not exists (select 1 from l_artist_recording lar 
                                where lar.entity1=rec.id and lar.entity0=a.id)
               group by a.gid order by count(*) desc;"""


def row(composer):
    return """<tr><td><a href="https://musicbrainz.org/artist/{}">{}</a></td><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>""".format(*composer, "{0:.2f}".format(pow(composer[5],2)/composer[4]))

mbz = pg.connect(host="mb", user="musicbrainz", database="musicbrainz_db")

with mbz.cursor() as cur:
    cur.execute("""select sort_name, gid from artist where gid in %s""", (tuple(composers),))
    db = { gid:dict({"name": name}) for name,gid in cur.fetchall() }

def do_query(table):
    with mbz.cursor() as cur:
        cur.execute(query.format(table=table), (tuple(composers),))
        res = cur.fetchall()
        return(res)

for butt in tables[:-1]:
    for gid,count in do_query(butt):
        db[gid][butt] = count

with mbz.cursor() as cur:
    cur.execute(rec_query, (tuple(composers),))
    for gid,count in cur.fetchall():
        db[gid]["recording"] = count

stuff = [ list([gid, db[gid]["name"], *[db[gid].get(butt,0) for butt in tables] ]) for gid in db.keys()]

with open("output/composers.html", "w") as fil:
    print(header, file=fil)
    for composer in sorted(stuff,key=lambda k: k[2],reverse=True):
        print(row(composer),file=fil)
    print("""</tbody></table></body></html>""",file=fil)
